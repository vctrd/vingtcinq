from rest_framework.pagination import PageNumberPagination


class DefaultPagination(PageNumberPagination):
    """
    Classe de pagination personnalisée pour définir les paramètres de pagination par défaut et permettre de modifier dynamiquement la taille de la page.
    """

    page_size = 25
    page_size_query_param = "page_size"
    max_page_size = 100
