from django.urls import path, include

from sales.urls import sales_router


urlpatterns = [
    path(
        "v1/",
        include(
            [
                path("auth/", include("dj_rest_auth.urls")),
                path("auth/registration/", include("dj_rest_auth.registration.urls")),
                path("", include(sales_router.urls)),
            ]
        ),
    )
]
