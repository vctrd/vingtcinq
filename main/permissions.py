from rest_framework import permissions


class IsOwnerOrCreateOnly(permissions.BasePermission):
    """
    Permission pour la lecture et la création d'objets seulement pour les utilisateurs authentifiés.
    Pour les objets avec un attribut `author`, seuls les auteurs peuvent les modifier.
    """

    def has_object_permission(self, request, view, obj):
        if (
            request.user
            and request.user.is_authenticated
            and request.method in ["POST", *permissions.SAFE_METHODS]
        ):
            return True

        if hasattr(obj, "author"):
            return obj.author == request.user

        return True
