from django.db.models import F, Subquery, OuterRef, Sum, Max, QuerySet

from .models import Article, Sale


def get_articles_with_data() -> QuerySet:
    """
    function pour obtenir les articles avec les données suivantes:
    - category_name
    - total_units_sold
    - total_value
    - total_cost
    - profit_margin
    - last_sale

    avec des subqueries pour diminuer les nombres de requetes
    """

    sales_quantity = Subquery(
        Sale.objects.filter(article_id=OuterRef("pk"))
        .values("article")
        .annotate(total_units_sold=Sum("quantity"))
        .values("total_units_sold")[:1]
    )
    sales_value = Subquery(
        Sale.objects.filter(article_id=OuterRef("pk"))
        .annotate(total_selling_price=F("quantity") * F("unit_selling_price"))
        .values("article")
        .annotate(total_value=Sum("total_selling_price"))
        .values("total_value")[:1]
    )
    last_sale = Subquery(
        Sale.objects.filter(article_id=OuterRef("pk"))
        .values("article")
        .annotate(last_sale=Max("date"))
        .values("last_sale")[:1]
    )

    return (
        Article.objects.annotate(
            category_name=F("category__display_name"),
            total_units_sold=sales_quantity,
            total_value=sales_value,
        )
        .annotate(
            total_cost=F("manufacturing_cost") * F("total_units_sold"),
        )
        .annotate(
            profit_margin=(F("total_value") - F("total_cost")) / F("total_cost"),
            last_sale=last_sale,
        )
        .order_by("-last_sale")
    )
