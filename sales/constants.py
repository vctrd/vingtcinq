ARTICLE_DOES_NOT_EXIST = "Article does not exist"
QUANTITY_POSITIVE_NOT_NULL = "Quantity must be greater than 0."
AUTHOR_REQUIRED = "Author is required."
