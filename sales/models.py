import re
from functools import partial

from django.core.validators import RegexValidator
from django.db import models


def MinValueConstraint(*, field, min_value):
    return models.CheckConstraint(
        name=f"%(app_label)s_%(class)s_{field}_gte_{min_value}",
        check=models.Q(**{field + "__gte": min_value}),
        violation_error_message=(f"{field} must be greater than or equal {min_value}."),
    )


PriceField = partial(models.DecimalField, max_digits=11, decimal_places=2)
DefaultCharField = partial(models.CharField, max_length=255)


class ArticleCategory(models.Model):
    """
    Category of an article
    """

    class Meta:
        verbose_name = "Article Category"
        verbose_name_plural = "Article Categories"

    objects = models.Manager()

    display_name = DefaultCharField("Display name", unique=True)

    def __str__(self):
        return f"{self.display_name}"


class Article(models.Model):
    """
    An article is an item that can be sold.
    """

    class Meta:
        verbose_name = "Article"
        verbose_name_plural = "Articles"
        constraints = [
            MinValueConstraint(field="manufacturing_cost", min_value=0),
        ]

    objects = models.Manager()

    code = models.CharField(
        "Code",
        max_length=6,
        unique=True,
        validators=[
            RegexValidator(
                regex=re.compile(r"[A-Z]{3}\d{3}"),
                message="Please use an ABC123 format.",
            )
        ],
    )
    category = models.ForeignKey(
        ArticleCategory,
        verbose_name="Category",
        related_name="articles",
        on_delete=models.PROTECT,
    )
    name = DefaultCharField("Name")
    manufacturing_cost = PriceField("Manufacturing Cost")

    def __str__(self):
        return f"{self.code} - {self.name}"


class Sale(models.Model):
    """
    A sale of an article.
    """

    class Meta:
        verbose_name = "Sale"
        verbose_name_plural = "Sales"
        constraints = [
            MinValueConstraint(field="quantity", min_value=1),
            MinValueConstraint(field="unit_selling_price", min_value=0),
        ]

    objects = models.Manager()

    date = models.DateField("Date")
    author = models.ForeignKey(
        "users.User",
        verbose_name="Author",
        related_name="sales",
        on_delete=models.PROTECT,
    )
    article = models.ForeignKey(
        Article, verbose_name="Article", related_name="sales", on_delete=models.CASCADE
    )
    quantity = models.IntegerField("Quantity")
    unit_selling_price = PriceField("Unit selling price")

    @property
    def total_selling_price(self):
        return self.unit_selling_price * self.quantity

    def __str__(self):
        return f"{self.date} - {self.quantity} {self.article.name}"
