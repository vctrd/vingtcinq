from rest_framework import viewsets

from .models import Sale
from .serializers import SaleSerializer, ArticleDataSerializer
from .selectors import get_articles_with_data


class SaleViewSet(viewsets.ModelViewSet):
    """
    Queryset pour les ventes avec l'ajout de select_related pour éviter les requêtes supplémentaires.
    """

    queryset = Sale.objects.all().select_related("article__category", "author")
    serializer_class = SaleSerializer


class ArticleDataViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = get_articles_with_data()
    serializer_class = ArticleDataSerializer
