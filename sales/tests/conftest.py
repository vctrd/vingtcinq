import pytest

from sales.models import Article, ArticleCategory, Sale


@pytest.fixture
def category():
    return ArticleCategory.objects.create(display_name="Test category")


@pytest.fixture
def article(category):
    return Article.objects.create(
        code="ABC123",
        name="Test article",
        manufacturing_cost=50,
        category=category,
    )


@pytest.fixture
def sale(article, user):
    return Sale.objects.create(
        author=user,
        date="2024-03-01",
        article=article,
        quantity=1,
        unit_selling_price=100,
    )
