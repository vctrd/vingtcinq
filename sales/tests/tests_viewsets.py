import pytest

from ..constants import ARTICLE_DOES_NOT_EXIST
from users.models import User


@pytest.mark.django_db
def test_create_sale(client, article):
    response = client.post(
        "/api/v1/sales/",
        {
            "date": "2024-03-01",
            "quantity": 5,
            "article_code": article.code,
            "unit_selling_price": 100,
        },
        format="json",
    )

    assert response.status_code == 201
    assert "id" in response.data


@pytest.mark.django_db
def test_create_sale_invalid_article(client):
    response = client.post(
        "/api/v1/sales/",
        {
            "date": "2024-03-01",
            "quantity": 5,
            "article_code": "INVALID",
            "unit_selling_price": 100,
        },
        format="json",
    )

    assert response.status_code == 400
    assert "article_code" in response.data
    assert response.data["article_code"][0] == ARTICLE_DOES_NOT_EXIST


@pytest.mark.django_db
def test_patch_sale(client, sale):
    response = client.patch(
        f"/api/v1/sales/{sale.id}/",
        {
            "quantity": 10,
        },
        format="json",
    )

    assert response.status_code == 200
    sale.refresh_from_db()
    assert sale.quantity == 10


@pytest.mark.django_db
def test_put_sale(client, sale, article):
    response = client.put(
        f"/api/v1/sales/{sale.id}/",
        {
            "date": "2024-03-01",
            "quantity": 5,
            "article_code": article.code,
            "unit_selling_price": 200,
        },
        format="json",
    )

    assert response.status_code == 200
    sale.refresh_from_db()
    assert sale.quantity == 5


@pytest.mark.django_db
def test_put_sale_unauthorized_user(client, sale, article):
    new_user = User.objects.create(email="new@email.com")
    sale.author = new_user
    sale.save()

    response = client.put(
        f"/api/v1/sales/{sale.id}/",
        {
            "date": "2024-03-01",
            "quantity": 5,
            "article_code": article.code,
            "unit_selling_price": 200,
        },
        format="json",
    )

    assert response.status_code == 403


@pytest.mark.django_db
def test_list_articles_with_data(client, article, sale):
    response = client.get("/api/v1/articles/")

    assert response.status_code == 200
    article_data = response.json()["results"][0]
    assert {
        "category_name",
        "total_units_sold",
        "total_value",
        "total_cost",
        "profit_margin",
        "last_sale",
    }.issubset(set(article_data.keys()))
