from rest_framework.routers import DefaultRouter

from .viewsets import SaleViewSet, ArticleDataViewSet

sales_router = DefaultRouter()
sales_router.register(r"sales", SaleViewSet)
sales_router.register(r"articles", ArticleDataViewSet)
