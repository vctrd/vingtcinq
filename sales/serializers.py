from django.utils.translation import gettext_lazy as _

from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied

from .models import Sale, Article, ArticleCategory
from .constants import (
    ARTICLE_DOES_NOT_EXIST,
    QUANTITY_POSITIVE_NOT_NULL,
    AUTHOR_REQUIRED,
)


class ArticleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ArticleCategory
        fields = [
            "id",
            "display_name",
        ]


class ArticleSerializer(serializers.ModelSerializer):
    category = ArticleCategorySerializer()

    class Meta:
        model = Article
        fields = [
            "id",
            "code",
            "category",
            "name",
            "manufacturing_cost",
        ]


class SaleSerializer(serializers.ModelSerializer):
    category = serializers.CharField(
        source="article.category.display_name", read_only=True
    )
    article_code = serializers.CharField(source="article.code", read_only=False)

    class Meta:
        model = Sale
        fields = [
            "id",
            "date",
            "category",
            "article_code",
            "quantity",
            "unit_selling_price",
            "total_selling_price",
        ]

    def validate_article_code(self, value):
        try:
            Article.objects.get(code=value)
        except Article.DoesNotExist:
            raise serializers.ValidationError(_(ARTICLE_DOES_NOT_EXIST))

        return value

    def validate_quantity(self, value):
        if value < 1:
            raise serializers.ValidationError(_(QUANTITY_POSITIVE_NOT_NULL))

        return value

    def create(self, validated_data):
        article = validated_data.pop("article")
        try:
            author = self.context["request"].user
        except (KeyError, AttributeError):
            raise serializers.ValidationError(_(AUTHOR_REQUIRED))

        return Sale.objects.create(
            author=author,
            article=Article.objects.get(code=article.get("code")),
            **validated_data,
        )

    def update(self, instance, validated_data):
        try:
            author = self.context["request"].user
        except (KeyError, AttributeError):
            raise serializers.ValidationError(_(AUTHOR_REQUIRED))
        if instance.author != author:
            raise PermissionDenied(_(AUTHOR_REQUIRED))

        article = validated_data.pop("article", None)
        if article:
            instance.article = Article.objects.get(code=article.get("code"))
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class ArticleDataSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField()
    total_units_sold = serializers.IntegerField()
    total_value = serializers.DecimalField(max_digits=10, decimal_places=2)
    total_cost = serializers.DecimalField(max_digits=10, decimal_places=2)
    profit_margin = serializers.DecimalField(max_digits=10, decimal_places=2)
    last_sale = serializers.DateField()

    class Meta:
        model = Article
        fields = [
            "id",
            "code",
            "category_name",
            "total_units_sold",
            "total_value",
            "total_cost",
            "profit_margin",
            "last_sale",
        ]
