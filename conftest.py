import pytest

from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token

from users.models import User


@pytest.fixture
def user():
    user = User.objects.create(email="test@email.com")
    user.set_password("password")
    user.save()
    return user


@pytest.fixture
def client(user):
    client = APIClient()
    token = Token.objects.create(user=user)
    client.credentials(HTTP_AUTHORIZATION=f"Token {token.key}")
    return client
